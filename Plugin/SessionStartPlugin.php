<?php
/**
 * Basic idea taken from https://github.com/magento/magento2/pull/14973 and implemented as Plugin
 *
 * The regular override of the class is not working as Magento core developers did not
 * implement SessionManagerInterface, but instead extends SessionManager
 *
 * @category    Scandiweb
 * @authors     Aleksej Tsebinoga <info@scandiweb.com> | Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\SessionFix\Plugin;

use Magento\Framework\App\Request\Http;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\Phrase;
use Magento\Framework\Profiler;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\Session\SidResolverInterface;
use Magento\Framework\Session\StorageInterface;
use Magento\Framework\Session\ValidatorInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Class SesssionStartPlugin
 * @package Scandiweb\Session\Plugin
 */
class SessionStartPlugin
{
    /**
     * Session key for list of hosts
     */
    const HOST_KEY = '_session_hosts';
    
    /** @var ValidatorInterface  */
    protected $validator;
    
    /** @var State  */
    protected $appState;
    
    /** @var SidResolverInterface  */
    protected $sidResolver;
    
    /** @var StorageInterface  */
    protected $storage;
    
    /** @var ConfigInterface  */
    protected $sessionConfig;
    
    /** @var SaveHandlerInterface  */
    protected $saveHandler;
    
    /** @var Http  */
    protected $request;
    
    /** @var CookieMetadataFactory  */
    protected $cookieMetadataFactory;
    
    /** @var CookieManagerInterface  */
    protected $cookieManager;
    
    /**
     * SesssionStartPlugin constructor.
     * @param SidResolverInterface $sidResolver
     * @param State $appState
     * @param ValidatorInterface $validator
     * @param StorageInterface $storage
     * @param ConfigInterface $sessionConfig
     * @param SaveHandlerInterface $saveHandler
     * @param Http $request
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param CookieManagerInterface $cookieManager
     */
    public function __construct(
        SidResolverInterface $sidResolver,
        State $appState,
        ValidatorInterface $validator,
        StorageInterface $storage,
        ConfigInterface $sessionConfig,
        SaveHandlerInterface $saveHandler,
        Http $request,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager
    )
    {
        $this->sidResolver = $sidResolver;
        $this->appState = $appState;
        $this->validator = $validator;
        $this->storage = $storage;
        $this->sessionConfig = $sessionConfig;
        $this->saveHandler = $saveHandler;
        $this->request = $request;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
    }
    
    /**
     * Plugin reimplementation of the start() from the solution
     *
     * @param SessionManager $subject
     * @param callable $proceed
     * @return mixed
     * @throws SessionException
     */
    public function aroundStart(SessionManager $subject, callable $proceed)
    {
        if (!$subject->isSessionExists()) {
            Profiler::start('session_start');
            
            try {
                $this->appState->getAreaCode();
            } catch (LocalizedException $e) {
                throw new SessionException(
                    new Phrase(
                        'Area code not set: Area code must be set before starting a session.'
                    ),
                    $e
                );
            }
            
            // Need to apply the config options so they can be ready by session_start
            $this->initIniOptions();
            $this->registerSaveHandler();
            if (isset($_SESSION['new_session_id'])) {
                // Not fully expired yet. Could be lost cookie by unstable network.
                session_commit();
                session_id($_SESSION['new_session_id']);
            }
            $sid = $this->sidResolver->getSid($subject);
            // potential custom logic for session id (ex. switching between hosts)
            $subject->setSessionId($this->sidResolver->getSid($subject));
            session_start();
            if (isset($_SESSION['destroyed'])) {
                if ($_SESSION['destroyed'] < time()-300) {
                    $subject->destroy(['clear_storage' => true]);
                    
                }
            }
            $this->validator->validate($subject);
            $this->renewCookie($sid, $subject);
            
            register_shutdown_function([$subject, 'writeClose']);
            
            $this->_addHost();
            Profiler::stop('session_start');
        }
        $this->storage->init(isset($_SESSION) ? $_SESSION : []);
        
        $returnValue = $proceed();
        return $returnValue;
    }
    
    /**
     * Plugin reimplementation of the regenerateId() from the solution
     *
     * @param SessionManager $subject
     * @param callable $proceed
     * @return mixed
     */
    public function aroundRegenerateId(SessionManager $subject, callable $proceed)
    {
        if (headers_sent()) {
            return $proceed();
        }
        
        if ($subject->isSessionExists()) {
            $oldSessionId = session_id();
            session_regenerate_id();   //regen the session
            $new_session_id = session_id();
            
            $_SESSION['new_session_id'] = $new_session_id;
            
            // Set destroy timestamp
            $_SESSION['destroyed'] = time();
            
            // Write and close current session;
            session_commit();
            $oldSession = $_SESSION;   //called after destroy - see destroy!
            // Start session with new session ID
            session_id($new_session_id);
            ini_set('session.use_strict_mode', 0);
            session_start();
            ini_set('session.use_strict_mode', 1);
            $_SESSION = $oldSession;
            // New session does not need them
            unset($_SESSION['destroyed']);
            unset($_SESSION['new_session_id']);
        } else {
            session_start();
        }
        $this->storage->init(isset($_SESSION) ? $_SESSION : []);
        
        if ($this->sessionConfig->getUseCookies()) {
            $this->clearSubDomainSessionCookie($subject);
        }
        
        return $proceed();
    }
    
    /**
     * Renew session cookie to prolong session
     *
     * @param null|string $sid If we have session id we need to use it instead of old cookie value
     * @param SessionManager $pluginSubject
     * @return $this
     */
    private function renewCookie($sid, SessionManager $pluginSubject)
    {
        if (!$pluginSubject->getCookieLifetime()) {
            return $this;
        }
        //When we renew cookie, we should aware, that any other session client do not
        //change cookie too
        $cookieValue = $sid ?: $this->cookieManager->getCookie($pluginSubject->getName());
        if ($cookieValue) {
            $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
            $metadata->setPath($this->sessionConfig->getCookiePath());
            $metadata->setDomain($this->sessionConfig->getCookieDomain());
            $metadata->setDuration($this->sessionConfig->getCookieLifetime());
            $metadata->setSecure($this->sessionConfig->getCookieSecure());
            $metadata->setHttpOnly($this->sessionConfig->getCookieHttpOnly());
            $this->cookieManager->setPublicCookie(
                $pluginSubject->getName(),
                $cookieValue,
                $metadata
            );
        }
        return $this;
    }

    /**
     * Expire the session cookie for sub domains
     * NOTE: Moved due the protected access in the target class
     *
     * @param SessionManager $pluginSubject
     * @return void
     */
    protected function clearSubDomainSessionCookie(SessionManager $pluginSubject)
    {
        foreach (array_keys($this->_getHosts()) as $host) {
            // Delete cookies with the same name for parent domains
            if (strpos($this->sessionConfig->getCookieDomain(), $host) > 0) {
                $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
                $metadata->setPath($this->sessionConfig->getCookiePath());
                $metadata->setDomain($host);
                $metadata->setSecure($this->sessionConfig->getCookieSecure());
                $metadata->setHttpOnly($this->sessionConfig->getCookieHttpOnly());
                $this->cookieManager->deleteCookie($pluginSubject->getName(), $metadata);
            }
        }
    }
    
    /**
     * Performs ini_set for all of the config options so they can be read by session_start
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return void
     */
    private function initIniOptions()
    {
        foreach ($this->sessionConfig->getOptions() as $option => $value) {
            $result = ini_set($option, $value);
            if ($result === false) {
                $error = error_get_last();
                throw new \InvalidArgumentException(
                    sprintf('Failed to set ini option "%s" to value "%s". %s', $option, $value, $error['message'])
                );
            }
        }
    }
    
    /**
     * Register save handler
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return bool
     */
    protected function registerSaveHandler()
    {
        return session_set_save_handler(
            [$this->saveHandler, 'open'],
            [$this->saveHandler, 'close'],
            [$this->saveHandler, 'read'],
            [$this->saveHandler, 'write'],
            [$this->saveHandler, 'destroy'],
            [$this->saveHandler, 'gc']
        );
    }
    
    /**
     * Register request host name as used with session
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return $this
     */
    protected function _addHost()
    {
        $host = $this->request->getHttpHost();
        if (!$host) {
            return $this;
        }
        
        $hosts = $this->_getHosts();
        $hosts[$host] = true;
        $_SESSION[self::HOST_KEY] = $hosts;
        return $this;
    }
    
    /**
     * Get all host names where session was used
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return array
     */
    protected function _getHosts()
    {
        return isset($_SESSION[self::HOST_KEY]) ? $_SESSION[self::HOST_KEY] : [];
    }
    
}
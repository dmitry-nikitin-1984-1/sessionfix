<?php
/**
 * Scandiweb_SessionFix
 *
 * @author Ilja Lapkovskis <info@scandiweb.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandiweb_SessionFix',
    __DIR__
);
# Magento 2 Session Bug Fix

Under some circumstances, like multiple checkout reload, Magento 2 may wipe-out client session, and create a new one,
 because of race-conditions and session being locked. In fact it leads to "Empty
 cart" page and ruins user experience.
 
## Solution
 
 Solution is based on publicly available materials, and just provides a clean and fast way of installing solution.
 
##Installation
 
1. Add the repo to composer.json repositories list:
```
   "repositories": [
   { 
        "type": "vcs",
        "url": "https://bitbucket.org/scandiweb/sessionfix.git"
    }
 ]
 ```
 
2. Require scandiweb/sessionfix: 
 
`composer require scandiweb/sessionfix`
 
3. Flush caches and enjoy
 
## Implementation
 The package is fixing the issue out of the box by registering a plugin that wraps original methods. According to the
  reported issue, the bug is present starting from Magento 2.1.0 and will be officially fixed in version 2.2.5/2.3.0, which is the reason why we have added limited Magento version requirements to our package.
  
## Having issues?
Please report them within the [issues](https://bitbucket.org/scandiweb/sessionfix/issues?status=new&status=open) 
section.